from future import standard_library

standard_library.install_aliases()
import requests, argparse, json, glob
from os import path as pth
from urllib.parse import urljoin


import tdtconfig

conf_dict = tdtconfig.read()

URL_QC = urljoin(conf_dict["URL_FWSS"], "/gps/data/qcfile")


def run(DIR):

    files = glob.glob(pth.join(DIR, "*"))
    failures = []
    for f in files:
        res = requests.post(URL_QC, data=open(f))
        print("File", f, "posted with result", res.status_code)
        if res.status_code not in [200, 201]:
            failures.append(f)
    if failures != []:
        return False
    else:
        return True
