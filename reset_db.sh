databasename=testdat
user=postgres
password=test2021
echo "Example to reset a database"
echo "do not use in production !"
echo "database used: $databasename"
echo "user: $user"
echo "continue ?"

#while [ true ] ; do
read res
if [ $res == 'y' ] ; then
	export PGPASSWORD=$password
	psql -U $user -h 127.0.0.1 -p 5442  -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$databasename' AND pid <> pg_backend_pid();"
	psql -U $user -h 127.0.0.1 -p 5442  -c "DROP DATABASE $databasename"
	psql -U $user -h 127.0.0.1 -p 5442  < ./$databasename.sql
	echo "db $databasename reset"
else

	echo "ok exit"
fi
#done

