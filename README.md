# Introduction

This tool allows to test a fresh installation of a GLASS. See [here](https://docs.google.com/document/d/1KYqQWBiNrz-d13n67akO6m3CDN4vY-BzQ9mkrNNgEzI/edit?usp=sharing) for more information.

**Author:**
Jean-Luc Menut (Jean-Luc.MENUT@geoazur.unice.fr)

**Version**
2.22.1

**Requirements:**
`python 3.8` 
`requests`
`future`

a requirements.txt is provided

**Configuration and Usage:**
- copy config.cfg.template to config.cfg
- configure config.cfg : replace \<url> by the url of the node, and \<port> by the port of the service. 
  - URL_FWSS is for the FWSS service (5555 by default)
  - URL_GLASSAPI_STATIONS and URL_GLASSAPI_FILES are for the glass framework service (8080 by default)
- Run `python test-tool.py`


Run `python test-tool.py -h` to display the help.
Run `python test-tool.py --version` to display the version of the software.
Run `python test-tool.py --type <type>` to run the test for a specific metadata type (station, file or QC).

To run the tool

`python test_tool.py --type=station`

`python test_tool.py --type=file`

`python test_tool.py --type=QC`

For each step a message will tell if successful or not.

Don't forget to reset you database after the test.
Also, it may be necessary to refresh the Glass Framework cache.
