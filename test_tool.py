from __future__ import print_function
from socket import if_nameindex
from future import standard_library

standard_library.install_aliases()
import requests, argparse, json, glob
from os import path as pth
from urllib.parse import urljoin
import requests

import upload_station_md
import upload_file_md
import upload_QC_md
import addAgencyAbr
import uploadDatacenter
import tdtconfig

__VERSION__ = "2.2"


def test_station_medata():
    # Test station metadata
    print("Test station metadata upload, modification and deletion")
    DIR = pth.join(conf_dict["DATA_DIR"], "test_node_station_md/")
    FILETEST = pth.join(
        pth.join(conf_dict["DATA_DIR"], "test_results"), "upload_station_md.json"
    )
    print("testing station md upload")
    print("=====================")
    status_code_list = upload_station_md.run(DIR)
    if upload_station_md.test_status_code(status_code_list):
        msg = "station md upload  is a success"
        result.append(msg)
    else:
        msg = "station md upload is a failure"
        result.append(msg)
        import sys

        sys.exit()
    print("=====================")

    print("testing station md modification")
    print("=====================")
    DIR = pth.join(conf_dict["DATA_DIR"], "test_node_station_md_modif/")
    # FILETEST = pth.join(pth.join(conf_dict['DATA_DIR'], 'test_results'), 'upload_station_md_modif.json')
    status_code_list = upload_station_md.run(DIR)
    if upload_station_md.test_status_code(status_code_list):
        msg = "station md Update is a success"
        result.append(msg)
    else:
        msg = "station md Update is a failure"
        result.append(msg)
        import sys

        sys.exit()
    print("=====================")

    print("testing station md deletion")
    print("=====================")
    CODE = "ADE000GLP"
    FILETEST = pth.join(
        pth.join(conf_dict["DATA_DIR"], "test_results"), "upload_station_md_delete.json"
    )
    status_code_list = upload_station_md.delete(CODE)
    if upload_station_md.test_status_code(status_code_list):
        msg = "station MD deletion is a success"
        result.append(msg)
    else:
        msg = "station MD deletion is a failure"
        result.append(msg)
        import sys

        sys.exit()
    print("=====================")


def test_file_metadata():
    print("testing file MD upload")
    print("=====================")
    addAgencyAbr.run()

    DC_FILE = pth.join(conf_dict["DATA_DIR"], "test_node_datacenter", "datacenter.json")
    uploadDatacenter.run(DC_FILE)

    DIR = pth.join(conf_dict["DATA_DIR"], "test_node_file_md")
    FILETEST = pth.join(
        pth.join(conf_dict["DATA_DIR"], "test_results"), "upload_file_md.json"
    )
    status_code_list = upload_file_md.run(DIR)

    if upload_file_md.test_status_code(status_code_list):
        msg = "file MD upload is a success"
        result.append(msg)
    else:
        msg = "file MD upload is a failure"
        result.append(msg)
        import sys

        sys.exit()


def test_QC_metadata():

    print("=====================")
    print("testing T3 upload")
    print("=====================")

    DIR = pth.join(conf_dict["DATA_DIR"], "test_node_QC_md")
    # FILETEST = pth.join(pth.join(conf_dict['DATA_DIR'], 'test_results'), 'upload_file_md.json')
    res = upload_QC_md.run(DIR)
    if res:
        msg = "QC upload is a success"
        result.append(msg)
    else:
        msg = "QC upload is a failure"
        print(msg)
        result.append(msg)
    print("=====================")

    print("=====================")
    print("Results")
    print("=====================")

    for i in result:
        print(i)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test a fresh GLASS node install")
    parser.add_argument(
        "--version",
        action="store_true",
        help="display the version. The program run normaly if not called",
    )
    parser.add_argument(
        "--type",
        action="store",
        help="select the type of metadata (station, file, QC). All metadata are tested if not called",
    )

    args = parser.parse_args()
    if args.version:
        print("Software version: ", __VERSION__)
        import sys

        sys.exit()

    result = []
    conf_dict = tdtconfig.read()
    if args.type:
        if args.type == "station" or not args.type:
            test_station_medata()
        elif args.type == "file" or not args.type:
            test_file_metadata()
        elif args.type == "QC" or not args.type:
            test_QC_metadata()
        else:
            print("Type not recognized")
            parser.print_help()
    else:
        test_station_medata()
        test_file_metadata()
        test_QC_metadata()
    print(20 * "!")
    print("/!\ Don't forget to reset the database")
    print(20 * "!")
