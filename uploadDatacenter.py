from __future__ import print_function
from future import standard_library

standard_library.install_aliases()
import requests, argparse, json, glob
from os import path as pth
from urllib.parse import urljoin
import tdtconfig

conf_dict = tdtconfig.read()
URL_DC = urljoin(conf_dict["URL_FWSS"], "gps/datacenter")


def run(dc_file):
    # parser = argparse.ArgumentParser(description='send datacenter file to DB-API')
    # parser.add_argument('file', help='file name')
    # args = parser.parse_args()

    if dc_file:
        if pth.exists(dc_file):
            dc = json.load(open(dc_file))
            for d in dc:

                res = requests.post(URL_DC, data=json.dumps(d))
                print(
                    "Datacenter {} posted with result {}".format(
                        d["name"], res.status_code
                    )
                )
        else:
            print(dc_file, "does not exist")
