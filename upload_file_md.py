from __future__ import print_function
from future import standard_library

standard_library.install_aliases()
import requests, argparse, json, glob
from os import path as pth
from urllib.parse import urljoin
import json

import tdtconfig

conf_dict = tdtconfig.read()

URL_RINEX = urljoin(conf_dict["URL_FWSS"], "/gps/data/rinex")
URL_GLASSAPI_FILES = conf_dict["URL_GLASSAPI_FILES"]


def run(DIR):
    status_code_list = []
    files = glob.glob(pth.join(DIR, "*"))
    for i in files:
        if pth.exists(i):

            rnx = json.loads(open(i).read())
            res = requests.post(URL_RINEX, data=open(i).read())
            # print(res.status_code)

            print("File {} posted with result {}".format(i, res.status_code))
            status_code_list.append(res.status_code)

        else:
            print(i, "does not exist")
    return status_code_list


def test_status_code(status_code_list):
    for i in status_code_list:
        if i not in [200, 201]:
            return False
    return True


def test(FILETEST):
    jsn = requests.get(URL_GLASSAPI_FILES)
    if jsn.status_code not in [200, 201]:
        print("unable to reach", URL_GLASSAPI_FILES)
        # import sys
        # sys.exit()
    else:
        jt = jsn.json()
        rf = json.load(open(FILETEST))
        diff = []
        # for s in rf_station:
        #     if s not in jt_station:
        #         diff.append(s)
        # if diff == []:
        #     print "T1 successfully imported"
        # else:
        #     print "failure to import T1"
        #     print "missing stations", diff

        if jt == rf:
            return True


# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(description='send T2 file to DB-API')
#     parser.add_argument('files', nargs = '+', help='file names')
#     args = parser.parse_args()

#     run()
