#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Read the config file for GLASS test tool
"""
from future import standard_library
standard_library.install_aliases()
import os
from os.path import join, abspath
from configparser import SafeConfigParser

def read(conf_file = None):
    if conf_file is None:
        dirname, filename = os.path.split(abspath(__file__))
        config = SafeConfigParser()
        config.read(join(dirname, 'config.cfg'))
        conf_dict = {
            'DATA_DIR': dirname,
            'URL_FWSS': config.get('URL', 'URL_FWSS'),
            'URL_GLASSAPI_STATION':  config.get('URL', 'URL_GLASSAPI_STATION'),
            'URL_GLASSAPI_FILES':  config.get('URL', 'URL_GLASSAPI_FILES'),
        }
    return conf_dict